const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const { promisify } = require('util');
const exec = promisify(require('child_process').exec);
const git = require('simple-git/promise');
const fs = require('fs');
const shell = require('shelljs');
const request = require('request');

process.on("uncaughtException", e => {
    console.log(e);
    process.exit(1);
});

process.on("unhandledRejection", e => {
    console.log(e);
    process.exit(1);
});


const app = express();
const port = parseInt(process.env.PORT, 10) || 8000;
app.set('port', port);

app.use(cors());
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit: 50000}));


// gitcli fetch
app.post('/gitCli/commits/fetch', async (req, res) => {
  const result = await getGitCommitsByUrl(req.body.url);
  res.send(result);
});

// git api fetch
app.post('/gitApi/commits/fetch', async (req, res) => {

  const url = 'https://api.github.com/repos/' + req.body.user + '/' + req.body.repo + '/commits';

  const options = {
    url: url,
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Accept-Charset': 'utf-8',
      'Content-Type': 'application/json',
      'User-Agent': 'request'
    }
  };

  request(options, async (error, response, body) => {
    if(response.statusCode != 200) {
      const result = await getGitCommitsByUrl(req.body.url);
      return res.send(result);
    } else {
      res.send(body);
    }
  });

});

const server = http.createServer(app);
server.listen(port);

async function getGitCommitsByUrl (url) {
  try {
    await ensureDirSync('foo');
    await git('foo').clone(url);
    await shell.cd('foo');
    const ls = await shell.ls();
    await shell.cd(ls);

    const commits = await exec("git log --oneline | nl -v0 | sed 's/^ \+/&HEAD~/'");
    await shell.cd('../..');
    await shell.rm('-rf', 'foo');

    return commits.stdout;
  } catch (err) {
    await exec('rm -r foo');
    return err;
  }
};

async function ensureDirSync (dirpath) {
  try {
    fs.mkdirSync(dirpath, { recursive: true })
  } catch (err) {
    if (err.code !== 'EEXIST') throw err
  }
}
