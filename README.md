# CODACY CHALLENGE

### Requirements
- Node v10.x
- npm v6.x


### Start
- run: npm install
- run: npm run start:dev
    - ALTERNATIVE
        - run: node app.js

### Available Routes
- see challenge.postman_collection.json

### TODO
- Parse results into object structure;
- Pagination for results;
- Error handling;
- Implement tests;
